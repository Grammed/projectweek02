package platecheckerpro.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class SightingNotification implements Serializable {
    static final long serialVersionUID = 123210;
    Address address;
    String description;
    LocalDateTime localDateTime;

    public SightingNotification(LocalDateTime autoOrManual, Address address, String description) {
        if (autoOrManual == null || address == null || description == null || description.length() == 0)
            throw new RuntimeException("Missing required SightingNotification properties");
        this.localDateTime = autoOrManual;
        this.description = description;
        this.address = address;

    }

    public void printListNotificationFields() {
        System.out.println(this.localDateTime);
        this.address.printListAddressFields();
        System.out.println("5.description: " + getDescription());
    }
    public void setAddress(Address InputAddress) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getLocalDateTime() {
        return this.localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s", address, description, localDateTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SightingNotification that = (SightingNotification) o;
        return address.equals(that.address) && description.equals(that.description) && localDateTime.equals(that.localDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, description, localDateTime);
    }
}
