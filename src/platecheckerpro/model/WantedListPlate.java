package platecheckerpro.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WantedListPlate implements Serializable {
    static final long serialVersionUID = 123210;
    private List<SightingNotification> sightingNotifications = new ArrayList<>();
    private String numberPlate;
    private String reason;
    private LocalDate inputDate;

    int notificationCount; // todo remove

    public WantedListPlate(String numberPlate, String reason) {

        if (numberPlate == null || numberPlate.length() == 0 || reason == null || reason.length() ==0)
            throw new RuntimeException("Missing required WantedListPlate properties");

        setInputDate();
        this.numberPlate = numberPlate;
        this.reason = reason;
        this.inputDate = getInputDate();
    }

    public boolean addSightingNotification(SightingNotification sightingNotification) {
        if (sightingNotification == null)
            System.out.println("Error @ addSightingNotification, null passed.");
        sightingNotifications.add(sightingNotification);
        return true;
    }

    public List<SightingNotification> getSightingsList() {
        return this.sightingNotifications;
    }

    public SightingNotification getSightingAtIndex(int index) {
        return sightingNotifications.get(index);
    }

    public int getTotalSightings() {
        return sightingNotifications.size();
    }

    public LocalDate getInputDate() {
        return inputDate;
    }

    public void setInputDate() {
        this.inputDate = LocalDate.now();
    }

    public void setInputDate(LocalDate userEditedDate) {
        this.inputDate = userEditedDate;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void printPlateFields() {
        System.out.println("1. input date -> " + getInputDate() + "\n2. numberplate -> " + getNumberPlate() +
                "\n3. reason -> " + getReason());
    }

    @Override
    public String toString() {
        return String.format("\033[31m|%-5d |\033[0m%-10s\033[31m |%-12s |%-100s|\033[0m",getTotalSightings(), numberPlate, inputDate, reason);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WantedListPlate that = (WantedListPlate) o;
        return notificationCount == that.notificationCount && sightingNotifications.equals(that.sightingNotifications) && numberPlate.equals(that.numberPlate) && reason.equals(that.reason) && inputDate.equals(that.inputDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sightingNotifications, numberPlate, reason, inputDate, notificationCount);
    }
}