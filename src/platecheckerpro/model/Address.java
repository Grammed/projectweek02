package platecheckerpro.model;

import java.io.Serializable;
import java.util.Objects;

public class Address implements Serializable {
    static final long serialVersionUID = 123210;
    String street;
    String postcode;
    String houseNumber;
    String localArea;


    public Address(String street, String houseNumber, String localArea, String postcode) {

        if (street == null || street.length() == 0 || houseNumber == null || houseNumber.length() == 0 ||
        localArea == null || localArea.length() == 0 || postcode == null || postcode.length() == 0 )
            throw new RuntimeException("Missing required Address properties");

        this.street = street;
        this.postcode = postcode;
        this.houseNumber = houseNumber;
        this.localArea = localArea;
    }

    public void printListAddressFields() {
        System.out.println("1. street: " + getStreet() + "\n2. postcode: " + getPostcode() +
                "\n3. house number: " + getHouseNumber() + "\n4. local area: " + getLocalArea() );
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getLocalArea() {
        return localArea;
    }

    public void setLocalArea(String localArea) {
        this.localArea = localArea;
    }

    @Override
    public String toString() {
        return String.format("(%s, %S, %s, %s )", street, postcode, houseNumber, localArea);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return street.equals(address.street) && postcode.equals(address.postcode) && houseNumber.equals(address.houseNumber) && localArea.equals(address.localArea);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, postcode, houseNumber, localArea);
    }
}