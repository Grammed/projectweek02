package platecheckerpro.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Database implements Serializable {
    static final long serialVersionUID = 123210;
    private List<WantedListPlate> wantedPlates = new ArrayList<>();
    private List<WantedListPlate> archivedPlates = new ArrayList<>();


    public boolean addWantedListPlate(WantedListPlate plate) {
        //todo validation is not null add check exists too
        if (plate == null)
            return false;

        if (alreadyWanted(plate.getNumberPlate()))
            return false;

        wantedPlates.add(plate);
        return true;
    }

    public boolean archiveWantedListPlate(WantedListPlate plate) {
        if (plate == null)
            return false;
        archivedPlates.add(plate);
        return true;
    }

    public boolean removeWantedListPlate(int index) {
        if(index < 0 || index > wantedPlates.size() - 1)
            System.out.println("Error: index out of range @ removeWantedListPlate(index) ");
        wantedPlates.remove(index);

        return true;
    }

    public int getIndex(String numberPlateToCheck) {
        if (numberPlateToCheck == null || numberPlateToCheck.isBlank()) {
            System.out.println("Error blank or null String @ getIndex(numberPlateToCheck)");
        }

        if (wantedPlates.stream().anyMatch(plate -> plate.getNumberPlate().equals(numberPlateToCheck))) {
            return wantedPlates.stream().map(WantedListPlate::getNumberPlate)
                    .collect(Collectors.toList())
                    .indexOf(numberPlateToCheck);
        }
        return -1;
    }

    public boolean alreadyWanted(String numberPlateToCheck) {
        if (numberPlateToCheck == null || numberPlateToCheck.isBlank()) {
            System.out.println("Error blank or null String @ alreadyWanted(numberPlateToCheck)");
        }
            return wantedPlates.stream().anyMatch(plate -> plate.getNumberPlate().equals(numberPlateToCheck));
    }

    public WantedListPlate getWantedListPlate(int index) {
        if (index < 0)
            System.out.println("Error, invalid index passed to getWantedListPlate(int)");

        /*System.out.println("Before sighting added size:" + wantedPlates.size() +
                " \nInside getWantedListPlate(int index) plate toString:");
        System.out.println("Empty Sightings list: " + wantedPlates.get(index).getSightingsList().toString());*/ // todo delete me
        return wantedPlates.get(index);
    }

    public void setWantedListPlate(WantedListPlate plate, int index) {
        if (plate == null)
            System.out.println("Error null plate @ setWantedListPlate(plate, index)");
        if (index < 0 || index > wantedPlates.size())
            System.out.println("Error nullpointer @ setWantedListPlate(plate, index)");
        try {
            wantedPlates.set(index, plate);
            System.out.println("Delete me from setWantedListPlate" + wantedPlates.get(index).getSightingsList().toString());
        } catch (IllegalArgumentException iae) {
            System.out.println("Unable to set the edits @setWantedListPlate" + iae.getMessage());
        }
    }

    public List<WantedListPlate> getListWantedPlates() {

        return wantedPlates;
    }

    public List<WantedListPlate> getListArchivedPlates() {

        return archivedPlates;
    }

    public List<WantedListPlate> getFilteredWantedPlates(Predicate<WantedListPlate> liveSearch) {

        return wantedPlates;
    }

    public List<WantedListPlate> getFilteredArchivedPlates(Predicate<WantedListPlate> archiveSearch) {

        return archivedPlates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Database database = (Database) o;
        return wantedPlates.equals(database.wantedPlates) && archivedPlates.equals(database.archivedPlates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wantedPlates, archivedPlates);
    }
}