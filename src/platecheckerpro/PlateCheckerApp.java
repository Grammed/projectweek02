package platecheckerpro;
// todo add edit methods for Sighting Notification - groundwork done for this but incomplete haved shown how
//  I would do this.
// todo add csv import export.
// todo add object lookup instead of num plate.
// todo add lambda date filters similar to bookshops to limit list prints
// todo add predicate filters for practice
// todo further options -> admin password for delete.
// todo last edit timestamp with id.

import platecheckerpro.model.Address;
import platecheckerpro.model.Database;
import platecheckerpro.model.SightingNotification;
import platecheckerpro.model.WantedListPlate;
import platecheckerpro.utilities.ConsoleInputTool;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static platecheckerpro.utilities.ConsoleInputTool.*;

public class PlateCheckerApp {

    Database database = new Database();
    private final ZoneId BELGIUM = ZoneId.of("Europe/Brussels");
    private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private final DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final static String DEFAULT_DATA_PATH = "data.ser";

    public static void main(String[] args) {
        PlateCheckerApp app = new PlateCheckerApp();
        app.startApp();
    }

    public void startApp() {
        boolean running = true;
        boolean choice;

        System.out.println("\n\n\033[36mWelcome to Platechecker Pro\033[0m");

        if (loadData(DEFAULT_DATA_PATH)) {
            System.out.println("data successfully loaded from data.ser\n");
        }
        if (askUserEnterOrFalse("\nload sample data? (enter)\ndon't load sample data (any key and enter)"))
            addSampleData();

        do {
            choice = askUserEnterOrFalse("\ncheck if a plate is on the wanted list? (enter)" +
                    "\nmanage the wanted list or quit? (any other key and enter)");
            if (choice) {
                checkPlates();
            } else {
                decisionEditAddOrQuit();
            }
        } while (running);
    }

    void addSampleData() {

        try {
            WantedListPlate loadedPlate0 = new WantedListPlate("ONE1", "10,000 value unpaid parking fines");
            WantedListPlate loadedPlate1 = new WantedListPlate("TWO2", "CaseID: 12524. Do not approach");
            WantedListPlate loadedPlate2 = new WantedListPlate("THREE3", "CaseID: 32564. Reported stolen xxxxdate");
            WantedListPlate loadedPlate3 = new WantedListPlate("FOUR4", "CaseID: 23523. Suspect activity");
            WantedListPlate loadedPlate4 = new WantedListPlate("FIVE5", "Car owner suspected of being unfaithful to PC Broziak");
            WantedListPlate loadedPlate5 = new WantedListPlate("SIX6", "CaseID: 32568. Reported stolen xxxxdate");
            WantedListPlate loadedPlate6 = new WantedListPlate("SEVEN7", "CaseID: 32577. Reported stolen xxxxdate");
            WantedListPlate loadedPlate7 = new WantedListPlate("EIGHT8", "CaseID: 32585. Reported stolen xxxxdate");
            WantedListPlate loadedPlate8 = new WantedListPlate("NINE9", "CaseID: 32590. Reported stolen xxxxdate");
            database.addWantedListPlate(loadedPlate0);
            database.addWantedListPlate(loadedPlate1);
            database.addWantedListPlate(loadedPlate2);
            database.addWantedListPlate(loadedPlate3);
            database.addWantedListPlate(loadedPlate4);
            database.addWantedListPlate(loadedPlate5);
            database.addWantedListPlate(loadedPlate6);
            database.addWantedListPlate(loadedPlate7);
            database.addWantedListPlate(loadedPlate8);
            System.out.println("sample data loaded.");
        } catch (Exception ex) {
            System.out.println("Error @ addSampleData()" + ex.getMessage());
        }
    }

    public boolean saveData(String path) {
        try (FileOutputStream fos = new FileOutputStream(path);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(database);
            System.out.println("Data saved to " + path);
            return true;
        } catch (IOException ioex) {
            System.out.println("Save error: " + ioex.getMessage());
        }
        return false;
    }

    public boolean loadData(String path) {
        try (FileInputStream fis = new FileInputStream(path);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            database = (Database) ois.readObject();
            return true;
        } catch (IOException ioex) {
            System.out.println("Load error: Could not load data from " + path + " -> " + ioex.getMessage());
        } catch (ClassNotFoundException cnfex) {
            System.out.println("Load error: Invalid data in file " + path + " -> " + cnfex.getMessage());
        }
        return false;
    }

    private void checkPlates() {
        boolean running = true;
        String input;
        int indexFound;

        do {
            input = getUserNumberPlate("\nenter numberplate to check or add notification\n(/// to stop)");
            if (!(input.equals("///"))) {
                indexFound = database.getIndex(input.toUpperCase());
                if (indexFound >= 0) {
                    System.out.println("\n\033[31m!!! " + input.toUpperCase() + " IS ON WANTED LIST!!!" +
                            "\n\033[0ma sighting notification is required.");
                    pressEnterToContinue();
                    inputSightingNotificationAddress(input, indexFound);
                    break;
                } else {
                    boolean choice = askUserEnterOrTrue(
                            "\n\033[32m" + input.toUpperCase() + "\033[0m is not on the wanted list." +
                                    "\n\nadd \033[32m" + input.toUpperCase() +
                                    "\033[0m to wanted list? (any other key and enter)\ncheck another plate (enter)");
                    if (choice)
                        addWantedListPlate(input);
                }
            } else if (input.equals("///"))
                running = false;
        } while (running);
    }

    private void inputSightingNotificationAddress(String input, int indexFound) {

        System.out.println("\nwhere is/was vehicle \033[31m" + input.toUpperCase() + "\033[0m?");
        String street = getUserString("street: ");
        String houseNumber = getUserString("nearest house number: ");
        String localArea = getUserString("name of locality: ");
        String postcode = getUserString("postcode: ");

        try {
            Address newAddress = new Address(street, houseNumber, localArea, postcode);
            addAddressToSightingNotification(newAddress, input, indexFound);
        } catch (IllegalArgumentException iae) {
            System.out.println("\nSomething went wrong when attempting to create " +
                    " the new address" + iae.getMessage());
        }
    }

    private void addAddressToSightingNotification(Address address, String input, int indexFound) {
        WantedListPlate retrievedPlate = database.getWantedListPlate(indexFound);
        LocalDateTime auto = null;
        LocalDateTime manual = null;
        boolean dateNotSuccessfullyEntered = true;
        boolean autoFilledDate = true;
        boolean failedFinalCheck;

        do {
            if (askUserEnterOrFalse("\nauto timestamp this sighting with time of your next enter press? (enter)" +
                    "\nenter time of sighting manually? (any other key and enter)")) {
                auto = LocalDateTime.now(BELGIUM);
                dateNotSuccessfullyEntered = false;
            } else {
                String simpleEntryDateTime = getUserSimpleDateTime();
                String readyToParse = checkSimpleEntryDateTime(simpleEntryDateTime);
                if (readyToParse != null) {
                    manual = LocalDateTime.parse(readyToParse, FORMATTER);
                    dateNotSuccessfullyEntered = false;
                    autoFilledDate = false;
                } else {
                    System.out.println("Error, there was a problem with the date entered, please try again.");
                    continue;
                }
            }
            LocalDateTime printToUser = auto == null ? manual : auto;
            failedFinalCheck = (askUserEnterOrTrue("\n" + printToUser + " is the time that this vehicle" +
                    " was witnessed at the location (enter)" +
                    "\n\n" + printToUser + " is the incorrect time and it needs to be changed (any other key and enter)"));
            if (failedFinalCheck) {
                dateNotSuccessfullyEntered = true;
                manual = null;
                auto = null;
            }

        } while (dateNotSuccessfullyEntered);

        String sightingDescription =
                getUserString("\nenter description/additional info for this sighting of: \033[31m"
                        + input.toUpperCase() + "\033[0m");

        if (autoFilledDate) {
            try {
                SightingNotification newSightingNotification = new SightingNotification(auto, address, sightingDescription);
                retrievedPlate.addSightingNotification(newSightingNotification);
                database.setWantedListPlate(retrievedPlate, indexFound);
                System.out.println("\033[32m\nthe sighting notification was added successfully\033[0m" +
                        "\nreturning to numberplate checking screen\n");
                pressEnterToContinue();
                checkPlates();
            } catch (IllegalArgumentException iae) {
                System.out.println(("Something went wrong when attempting to add new sighting notification and" +
                        " set the edited Wanted List Plate (AUTO)"));
            }
        } else {
            try {
                SightingNotification newSightingNotification = new SightingNotification(manual, address, sightingDescription);
                retrievedPlate.addSightingNotification(newSightingNotification);
                database.setWantedListPlate(retrievedPlate, indexFound);
                System.out.println("\033[32m\nthe sighting notification was added successfully\033[0m" +
                        "\nReturning to numberplate checking screen.\n");
                pressEnterToContinue();
                checkPlates();
            } catch (IllegalArgumentException iae) {
                System.out.println(("Something went wrong when attempting to add new sighting notification and" +
                        " set the edited Wanted List Plate (MANUAL)"));
            }
        }
    }

    private void decisionEditAddOrQuit() {
        boolean running = true;
        int choice;

        do {
            choice = askUserInputInteger("\n\nenter choice 0 - 3\n1. manage wanted list\n2. add plate to wanted list" +
                    "\n3. check plates\n0. quit", 0, 3);
            switch (choice) {
                case 0:
                    checkSaveThenExit();
                    break;
                case 1:
                    decisionOverviewArchiveEdit();
                    break;
                case 2:
                    addWantedListPlate();
                    break;
                case 3:
                    checkPlates();
                    break;
                default:
                    System.out.println("something went wrong with decisionEditAddOrQuit switch...");
            }
        } while (running);
    }

    private void checkSaveThenExit() {

        if (askUserEnterOrFalse("\nsave data? (enter)\ndont save? (any key and enter)")) {
            if (saveData(DEFAULT_DATA_PATH)) {
                System.out.println("...successfully");
                System.exit(0);
            } else {
                System.out.println("unable to save data");
                System.exit(666);
            }
        } else {
            System.exit(0);
        }


    }

    private void addWantedListPlate(String input) {
        input = input.trim().toUpperCase();
        boolean stayInMethod = true;
        boolean confirmedGoAhead = false;

        do {
            if (askUserEnterOrFalse("\033[31m\nCONFIRM ACTION: " +
                    "\n\033[0mnumberplate: \033[31m\n" + input + "\n\033[0mwill be added to the wanted list." +
                    "\n\nmake correction? (enter) \nconfirm action? (any other key and enter)"))
                break;

            String reason =
                    getUserString("\n\nenter reason for\033[31m " + input.toUpperCase() +
                            "\033[0m being on the wanted list: ", false);

            System.out.println("\n" + reason);
            confirmedGoAhead = askUserEnterOrFalse("\n\033[31m" + input + "\033[0m " +
                    "will be added with reason above? (enter)\ncancel? (any other key and enter)");

            try {
                if (confirmedGoAhead) {
                    WantedListPlate newWantedListPlate = new WantedListPlate(input, reason);
                    database.addWantedListPlate(newWantedListPlate);
                    System.out.println(input + " successfully added to wanted list");
                    stayInMethod = false;
                } else {
                    stayInMethod = false;
                }
            } catch (IllegalArgumentException iae) {
                System.out.println(("Something went wrong when attempting to add new wanted list plate" +
                        "(directly from checking environment)"));
            }
        } while (stayInMethod);
    }

    private void addWantedListPlate() {
        boolean stayInMethod = true;
        boolean plateConfirmedByUser = false;
        boolean confirmedGoAhead;
        int count = 0;
        String numberPlate;

        do {
            if (count > 0)
                if (askUserEnterOrTrue("\nadd another plate? (enter) " +
                        "\nback to last menu? (any other key and enter)"))
                    break;

            do {

                numberPlate = getUserNumberPlate
                        ("\n\nenter numberplate to add to wanted list:");
                if (database.alreadyWanted(numberPlate)) {
                    System.out.println("\nplate already on the wanted list! returning to main menu.");
                    decisionEditAddOrQuit();
                }

                boolean carryOn = askUserEnterOrTrue("\033[31m\nCONFIRM ACTION: " +
                        "\n\033[0mnumberplate \033[31m\n" + numberPlate + "\n\033[0mwill be added to the wanted list." +
                        "\n\nmake correction? (enter)\nConfirm action and continue? (any other key and enter): ");

                if (carryOn)
                    plateConfirmedByUser = true;

            } while (!plateConfirmedByUser);

            String reason =
                    getUserString("enter reason for\033[31m " + numberPlate +
                            "\033[0m being on the wanted list: ", false);

            System.out.println("\n" + reason);
            confirmedGoAhead = askUserEnterOrFalse("\n\033[31m" + numberPlate + "\033[0m " +
                    "will be added with reason above? (enter)\nCancel? (any other key and enter) ");
            try {

                if (confirmedGoAhead) {
                    WantedListPlate newWantedListPlate = new WantedListPlate(numberPlate, reason);
                    database.addWantedListPlate(newWantedListPlate);
                    count++;
                } else {
                    stayInMethod = false;
                }
            } catch (IllegalArgumentException iae) {
                System.out.println(("Something went wrong when attempting to add new wanted list plate" +
                        "(from add menu)"));
            }
        } while (stayInMethod);
    }

    private void decisionOverviewArchiveEdit() {
        boolean running = true;
        int choice;

        do {
            choice = askUserInputInteger("\nenter choice 0 - 5\n1. view wanted list/specific plate\n2. delete/archive plates\n" +
                    "3. edit plates\n4. check plates\n5. view archive list plates (warning, may be very large)\n" +
                    "0. back to previous menu", 0, 5);
            switch (choice) {
                case 0:
                    running = false;
                    break;
                case 1:
                    interrogateWantedList(); //view current plates and their sighting notifications
                    break;
                case 2:
                    deleteOrArchivePlate(); //view current and archived plates archive current plates.
                    break;
                case 3:
                    boolean editChoice;
                    editChoice = (askUserEnterOrFalse("\n\nedit wanted list numberplate? (enter)" +
                            "\nedit sighting notifications linked to numberplate (any other key and enter)"));
                    if (editChoice) {
                        editWantedListPlate();
                    } else {
                        System.out.println("This is a stub, proof of concept shown with working numberplate" +
                                " edit options");
                    }
                    break;
                case 4:
                    running = false;
                    checkPlates();
                    break;
                case 5:
                    ArrayList<WantedListPlate> temp = new ArrayList<>(database.getListArchivedPlates());
                    System.out.println("Archive of previously wanted plates (duplicates allowed):\n");
                    temp.forEach(System.out::println);
                    break;
                default:
                    System.out.println("something went wrong with decisionEditAddOrQuit switch...");
            }
        } while (running);

    }

    private void interrogateWantedList() {
        boolean running = true;
        int choice;
        List<WantedListPlate> temp = new ArrayList<>(database.getListWantedPlates());
        System.out.println("Current Wanted List");
        temp.forEach(System.out::println);
        System.out.println();

        do {
            choice = askUserInputInteger("\nenter choice 0 - 3\n1. view specific plate notifications" +
                    "\n2. view list (only) plates with notifications\n3. view list (all) wanted list plates" +
                    "\n0. back to previous menu", 0, 3);
            switch (choice) {
                case 0:
                    running = false;
                    break;
                case 1:
                    viewSpecificPlateDetail();

                    break;
                case 2:
                    System.out.println("Wanted List plates with sighting notifications: ");
                    temp.stream().filter(plate -> plate.getTotalSightings() > 0)
                            .collect(Collectors.toList())
                            .forEach(System.out::println);

                    break;
                case 3:
                    System.out.println("Wanted List, in order of entry:");
                    temp.forEach(System.out::println);
                    break;
                default:
                    System.out.println("something went wrong with decisionEditAddOrQuit switch...");
            }
        } while (running);
    }

    private void viewSpecificPlateDetail() {
        List<WantedListPlate> temp = new ArrayList<>(database.getListWantedPlates());
        boolean noMatchChoice;
        boolean stayInMethod = true;
        int index;
        WantedListPlate retrievedPlate;

        if (ConsoleInputTool.askUserEnterOrTrue("\ninput numberplate directly (enter)" +
                "\nor view list first (any other key and enter): ")) {
            System.out.println("Wanted List, in order of entry:");
            temp.forEach(System.out::println);
        }

        do {
            String plateInput = ConsoleInputTool.getUserNumberPlate("Enter numberplate: ");

            if (database.alreadyWanted(plateInput)) {
                index = database.getIndex(plateInput);
                retrievedPlate = database.getWantedListPlate(index);
                System.out.println(retrievedPlate.toString());
                List<SightingNotification> tempB = new ArrayList<>(retrievedPlate.getSightingsList());
                for (SightingNotification sn : tempB)
                    System.out.println(sn.toString());
                pressEnterToContinue();
                stayInMethod = false;
            } else {
                noMatchChoice = askUserEnterOrFalse("plate entered " + plateInput + " is not on wanted list." +
                        "\ntry again? enter\ncancel? any other key and enter");
                if (noMatchChoice)
                    continue;
                stayInMethod = false;
                System.out.println("Returning you to management menu.");
            }

        } while (stayInMethod);
    }

    private void deleteOrArchivePlate() { boolean stayInMethod = true;
        String plateInput;
        int deleteArchiveIndex;
        int choice;
        boolean noMatchChoice;
        List<WantedListPlate> temp = new ArrayList<>(database.getListWantedPlates());
        System.out.println("Current Wanted List");
        temp.forEach(System.out::println);
        System.out.println();

        do {
            plateInput = getUserNumberPlate("input numberplate for archiving/deletion");
            if (database.alreadyWanted(plateInput)) {
                deleteArchiveIndex = database.getIndex(plateInput);
                choice = askUserInputInteger("\n\nenter choice 0 - 3\n1. archive " + plateInput + "\n2. delete " + plateInput +
                        "\n3. try again\n0. exit", 0, 3);
                switch (choice) {
                    case 0:
                        stayInMethod = false;
                        break;
                    case 1:
                        try {
                            WantedListPlate toArchive = database.getWantedListPlate(deleteArchiveIndex);
                            if (database.archiveWantedListPlate(toArchive)) {
                                database.removeWantedListPlate(deleteArchiveIndex);
                                if (askUserEnterOrFalse("\n\nplate successfully archived\n" +
                                        "\narchive/delete another plate? (enter)" +
                                        "\nreturn to previous menu? (any other key and enter)"))
                                    break;
                                stayInMethod = false;
                                System.out.println("returning to previous menu.");
                            } else
                                System.out.println("Error, archiveWantedListPlate(WLP) returned false. " +
                                        "\nThere was a problem with archiving");
                        } catch (Exception ex) {
                            System.out.println("deleteArchivePlate case1 failed" + ex.getMessage());
                        }
                        break;
                    case 2:
                        try {
                            if (database.removeWantedListPlate(deleteArchiveIndex)) {
                                if (askUserEnterOrFalse("plate successfully deleted\n" +
                                        "\narchive/delete another plate? (enter)" +
                                        "\nreturn to previous menu? (any other key and enter)"))
                                    break;
                                stayInMethod = false;
                                System.out.println("returning to previous menu");
                            } else
                                System.out.println("Error, removeWantedListPlate() returned false. " +
                                        "\nthere was a problem with deleting");
                        } catch (Exception ex) {
                            System.out.println("deleteOrArchivePlate() case 2 failed" + ex.getMessage());
                        }
                        break;
                    case 3:
                        break;
                }

            } else {
                noMatchChoice = askUserEnterOrFalse("\n\nnumberplate: " + plateInput + " is not on wanted list." +
                        "\ntry again? (enter)\ncancel? (any other key and enter)");
                if (noMatchChoice)
                    continue;
                stayInMethod = false;
                System.out.println("returning to management menu.");
            }

        } while (stayInMethod);
    }

    private void editWantedListPlate() {
        boolean stayInMethod = true;
        boolean invalidPlate = true;
        boolean editedPlate;
        boolean acceptEdit = false;
        int plateToEditIndex;
        String input;
        int fieldChoice;
        String readyToParse;
        LocalDate newDate;
        String newPlate;
        String newReason;
        String oldReason;
        ArrayList<WantedListPlate> temp = new ArrayList<>(database.getListWantedPlates());

        do {
            do {
                System.out.println("\nWanted List:");
                temp.forEach(System.out::println);
                input = getUserNumberPlate("\nEnter numberplate you want to edit (/// to exit to previous menu)");
                if (input.equals("///")) {
                    stayInMethod = false;
                    break;
                }
                if (database.alreadyWanted(input)) {
                    invalidPlate = false;
                } else {
                    System.out.println("\nplate not in list");
                }
            } while (invalidPlate);

            if (!stayInMethod)
                break;
            else {
                plateToEditIndex = database.getIndex(input);
                WantedListPlate toEdit = database.getWantedListPlate(plateToEditIndex);
                System.out.println("enter choice 1 - 3");
                toEdit.printPlateFields();
                fieldChoice = askUserInputInteger("->", 1, 3);

                switch (fieldChoice) {

                    case 1:
                        boolean stayInCase1 = true;
                        String formattedDate;
                        do {
                            readyToParse = getUserSimpleDate();
                            formattedDate = checkSimpleEntryDate(readyToParse);
                            acceptEdit = (askUserEnterOrFalse("accept edit? " +
                                    toEdit.getInputDate() + " -> " + formattedDate + " (enter)\ntry again? " +
                                    "(any other key and enter)"));
                            if (acceptEdit) {
                                try {
                                    newDate = LocalDate.parse(formattedDate, FORMATTER_DATE);
                                    toEdit.setInputDate(newDate);
                                    System.out.println("edit successful new local date set " + toEdit.getInputDate());
                                    stayInCase1 = false;
                                    stayInMethod = false;
                                } catch (Exception ex) {
                                    System.out.println("Error @ editWantedListPlate case 1 date" + ex.getMessage());
                                }
                            }
                        } while (stayInCase1);
                        break;

                    case 2:
                        boolean stayInCase = true;
                        do {
                            newPlate = getUserNumberPlate("enter new plate ->");
                            editedPlate = database.alreadyWanted(newPlate);
                            if (editedPlate) {
                                System.out.println(newPlate + " already on wanted list");
                                continue;
                            } else {
                                acceptEdit = (askUserEnterOrFalse("accept edit? " +
                                        toEdit.getNumberPlate() + " -> " + newPlate + " (enter)\ntry again? " +
                                        "(any other key and enter)"));
                                if (acceptEdit) {
                                    try {
                                        toEdit.setNumberPlate(newPlate);
                                        System.out.println("edit successful");
                                        stayInCase = false;
                                        stayInMethod = false;
                                    } catch (Exception ex) {
                                        System.out.println("Error @ editWantedListPlate case 2" + ex.getMessage());
                                    }
                                } else
                                    continue;
                            }
                        } while (stayInCase);
                        break;

                    case 3:
                        boolean stayInCase3 = true;
                        do {
                            oldReason = toEdit.getReason();
                            boolean fullyReplace;
                            System.out.println(oldReason);
                            fullyReplace = askUserEnterOrFalse("\nfully replace current reason? (enter)" +
                                    "\nadd text to current reason? (any other key and enter)");
                            newReason = getUserString("enter new text:");

                            acceptEdit = (askUserEnterOrFalse("accept edit? " +
                                    oldReason + " -> " + newReason + " (enter)\ntry again? " +
                                    "(any other key and enter)"));
                            if (acceptEdit) {
                                try {
                                    if (fullyReplace) {
                                        toEdit.setReason(newReason);
                                        stayInCase3 = false;
                                        stayInMethod = false;
                                    } else {
                                        toEdit.setReason(oldReason + " [later addition] " + newReason);
                                        System.out.println("edit successful reason updated\n" + toEdit.getReason());
                                        stayInCase3 = false;
                                        stayInMethod = false;
                                    }
                                } catch (Exception ex) {
                                    System.out.println("Error @ editWantedListPlate case 1 date" + ex.getMessage());
                                }
                            }
                        } while (stayInCase3);
                        break;
                    default:
                        System.out.println("Something went wrong in switch for WantedPlate edits.");
                }
            }
        } while (stayInMethod);
    }
}